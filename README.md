# Projet Signal

## Description
Ce projet est une refonte de la page d'accueil de Signal, réalisée avec le framework Angular. L'objectif est de recréer les fonctionnalités et le design de la page d'accueil de Signal en utilisant les outils et composants Angular.

## Prérequis
- Node.js
- Angular CLI

## Installation
1. Tout d'abord, clonez le projet depuis GitLab : git clone https://gitlab.com/flojarno/signal.git
2. Naviguez vers le répertoire du projet : cd signal
3. Installez les dépendances : npm install

## Lancer l'application
Pour démarrer l'application, exécutez : ng serve --open

Cela ouvrira automatiquement l'application dans votre navigateur web par défaut à l'adresse `http://localhost:4200/`.