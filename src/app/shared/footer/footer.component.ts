import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';

interface footerLink {
  name: string;
  url: string;
}

@Component({
  selector: 'app-footer',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './footer.component.html',
  styleUrl: './footer.component.css'
})

// 3 façons de typer un tableau :
//
// 1. Sans typage !
// 2. En typant les éléments du tableaux
// 3. En créant une interface


export class FooterComponent {

  // 1. Sans typage !
  organizationLinks = [
    { name: 'Donate', url: 'https://signal.org/fr/donate/' },
    { name: 'Careers', url: 'https://signal.org/workworkwork/' },
    { name: 'Blog', url: 'https://signal.org/blog/' },
    { name: 'Terms & Privacy Policy', url: 'https://signal.org/legal/' }
  ];

  // 2. En typant les éléments du tableaux
  downloadLinks: { name: string; url: string; }[] = [
    { name: 'Android', url: 'https://signal.org/fr/donate/' },
    { name: 'iPhone & iPad', url: 'https://signal.org/workworkwork/' },
    { name: 'Windows', url: 'https://signal.org/blog/' },
    { name: 'Mac', url: 'https://signal.org/blog/' },
    { name: 'Linux', url: 'https://signal.org/legal/' }
  ];
  
  // 3. En créant une interface
  socialLinks: footerLink[] = [
    { name: 'GitHub', url: 'https://signal.org/fr/donate/' },
    { name: 'Twitter', url: 'https://signal.org/workworkwork/' },
    { name: 'Instagram', url: 'https://signal.org/blog/' },
  ];

  helpLinks = [
    { name: 'Support Center', url: 'https://signal.org/fr/donate/' },
    { name: 'Community', url: 'https://signal.org/workworkwork/' },
  ];
}
