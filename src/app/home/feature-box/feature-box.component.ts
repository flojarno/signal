import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-feature-box',
  standalone: true,
  imports: [],
  templateUrl: './feature-box.component.html',
  styleUrl: './feature-box.component.css'
})
export class FeatureBoxComponent {
  @Input() boxImage: string = '';
  @Input() boxTitle: string = '';
  @Input() boxContent: string = '';
}
